# https://django-mptt.readthedocs.io/en/latest/tutorial.html
# https://django-mptt.readthedocs.io/en/latest/tutorial.html#template
# https://django-mptt.readthedocs.io/en/latest/forms.html

from django.shortcuts import render, reverse, HttpResponseRedirect, redirect
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from data_app.models import File
from data_app.forms import FileForm, LoginForm


# Create your views here.
def add_file(request):
    if request.method == 'POST':
        form = FileForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            file = File.objects.create(
                name=data.get('name'),
                parent=data.get('parent'),
                # flag=data.get('flag')
            )
            return HttpResponseRedirect(request.GET.get('next', reverse('homepage')))

    form = FileForm()
    return render(request, 'generic_form.html', {'form': form})


def index(request):
    data = File.objects.all()
    return render(request, 'index.html', {'data': data})


def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('homepage')

    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})


def login_view(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            user = authenticate(
                request, username=data['username'], password=data['password']
            )
            if user:
                login(request, user)

                return HttpResponseRedirect(request.GET.get('next', reverse('homepage')))

    form = LoginForm()
    return render(request, 'login.html', {'form': form})


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('login'))
