from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from django.contrib.auth.models import User


# Create your models here.
class File(MPTTModel):
    name = models.CharField(max_length=100, unique=True)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    # jalon helped with this & answered my hundreds of questions. lol
    flag = models.CharField(max_length=10, choices=[("File", "File"), ("Folder", "Folder")])

    class MPTTMeta:
        order_insertion_by = ['name']

    def __str__(self):
        return self.name
