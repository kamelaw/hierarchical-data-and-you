# https://django-mptt.readthedocs.io/en/latest/forms.html

from django import forms
from data_app.models import File
from mptt.forms import TreeNodeChoiceField
from django.contrib.auth.models import User


# class FileForm(forms.Form):
#     name = forms.CharField(max_length=100)
#     parent = TreeNodeChoiceField(queryset=File.objects.all())
# Jalon answered hundreds more questions here
class FileForm(forms.ModelForm):
    class Meta:
        model = File
        fields = ["name", "parent", "flag"]


class LoginForm(forms.Form):
    username = forms.CharField(max_length=200)
    password = forms.CharField(widget=forms.PasswordInput)


# class SignupForm(UserCreationForm):
#     class Meta:
#         model = User
