from django.contrib import admin
from mptt.admin import DraggableMPTTAdmin
from data_app.models import File


# Register your models here.
admin.site.register(File, DraggableMPTTAdmin)
